package org.jim.server.demo.service;

import org.jim.core.packets.LoginReqBody;
import org.jim.core.packets.User;
import org.jim.server.demo.dto.R;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * @author: thx
 * @date: 2022/8/7 15:10
 */
public interface RemoteApi {

    @GET("v1/findImUser")
    Call<R<User>> getUser(@Query("userName") String userName,@Query("type") String type);

    @POST("v1/doLoginImUser")
    Call<R> doLogin(@Body LoginReqBody loginReqBody);

}
