package org.jim.server.demo.dto;

import lombok.Data;

/**
 * @author: thx
 * @date: 2022/8/7 15:20
 */
@Data
public class R<T> {
    String code;
    String msg;
    T data;

    public boolean isSuccess() {
        return "0".equals(code);
    }
}
