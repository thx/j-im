package org.jim.server.demo.exception;

/**
 * @author: thx
 * @date: 2022/8/7 15:25
 */
public class ApiException extends RuntimeException {
    String code;
    String msg;

    public ApiException(String code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }
}
